# Valut2Go Application

This project concerns a system for users to manages their file system and take care of backing them up safely based on user preferences. To use the system called Vault2Go, users would select their back preferences such as directories to be backed up, file types to backup, backup frequency (such as Hourly, Daily, Weekly, Monthly), number of revisions to keep, back up provider (such as External Storage device mounted to the system, Google drive, Microsoft one drive, etc.). Vault2Go would keep track of the files changes using an index file that would be stored in the user’s system. The focus of the term project would be to create the backup system, which would backup users file automatically on an hourly basis to a internally mounted file system. 

## Getting Started

* Github location for this project - https://ashwin2202@bitbucket.org/ashwin2202/vault2go-app
* Clone the project from Github or download the source from the page
* This application uses **Maven** to manage the code build.
* Open the terminal and cd into vault2go-app
* Run the following command to compile the code and run the unit tests

			`mvn clean install`

* Import the application into eclipse using the following command

			`mvn eclipse:eclipse `

* Open Vault2GoStarter.java
* Run the main method

## Possible Output

```
		Welcome to Vault2Go applicaition!!!

Please answer few simple questions to create your own vault


Please provide the source directory
---------********** HINT **********---------
** For Windows machine start with the Drive name. Example: C:/user/folder
** For non-Windows machine start with the slash(/). Example: /user/folder
---------********** |||| **********---------
/Users⁩/aswinkumar⁩/Documents⁩/BU⁩/CS622⁩/backup

Error Id: 3000 | Error Message: Invalid directory provided. Please try again.

Please provide the source directory
---------********** HINT **********---------
** For Windows machine start with the Drive name. Example: C:/user/folder
** For non-Windows machine start with the slash(/). Example: /user/folder
---------********** |||| **********---------
/Users/aswinkumar/Documents/BU/CS622/src/vault2go-app
Please provide the destination directory
---------********** HINT **********---------
** For Windows machine start with the Drive name. Example: C:/user/folder
** For non-Windows machine start with the slash(/). Example: /user/folder
---------********** |||| **********---------
/Users/aswinkumar/Documents/BU/CS622/src/vault2go-app
1. Daily Backup runs every 1440 minute/s
2. Hourly backup runs every 60 minute/s
3. Instant runs every 0 minute/s

3

BackupPreference Created by user: 

id:  d0792366-73ef-41f5-8034-115014f6311d, 
frequency:  Instant, 
sourceFolder:  /Users/aswinkumar/Documents/BU/CS622/src/vault2go-app, 
destinationFolder:  /Users/aswinkumar/Documents/BU/CS622/src/vault2go-app

pool-1-thread-3 - ENTRY_MODIFY: /Users/aswinkumar/Documents/BU/CS622/src/vault2go-app/README.md
pool-1-thread-3 - ENTRY_MODIFY: /Users/aswinkumar/Documents/BU/CS622/src/vault2go-app/logs/history.log
```