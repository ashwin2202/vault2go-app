package com.vault2go.preferences;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

import com.vault2go.frequency.Frequencies;

public class PreferenceHandlerTest {
	
	private PreferenceHandler preferenceHandler;
	
	@Before
	public void setUp() throws Exception {
		preferenceHandler = new PreferenceHandler(Frequencies.getSupportedFrequencies());
	}

	@Test
	public void testSourceFolder() throws IOException {
		String in = Paths.get("docs").toAbsolutePath().toString();
		preferenceHandler.setScanner(new Scanner(in));
		assertEquals(in, preferenceHandler.fetchPathFromUser("source").toString());
	}
	
	@Test
	public void testSourceFolder2() throws IOException {
		String out = Paths.get("docs").toAbsolutePath().toString();
		String in = "/users/test"+ System.getProperty("line.separator")+out;
		preferenceHandler.setScanner(new Scanner(in));
		assertEquals(out, preferenceHandler.fetchPathFromUser("source").toString());
	}
	
	@Test	
	public void testDestinationFolder() throws IOException {
		String in = Paths.get("docs").toAbsolutePath().toString();
		preferenceHandler.setScanner(new Scanner(in));
		assertEquals(in, preferenceHandler.fetchPathFromUser("destination").toString());
	}
	
	@Test	
	public void testDestinationFolder2() throws IOException {
		String out = Paths.get("docs").toAbsolutePath().toString();
		String in = "/users/test"+ System.getProperty("line.separator")+out;
		preferenceHandler.setScanner(new Scanner(in));
		assertEquals(out, preferenceHandler.fetchPathFromUser("destination").toString());
	}
	
	@Test	
	public void testFrequencyIndex() throws IOException {
		String in = "1";
		preferenceHandler.setScanner(new Scanner(in));
		assertEquals(0, preferenceHandler.fetchFrequencyIndex());
	}
	
	@Test	
	public void testFrequencyIndex2() throws IOException {
		String in = "4"+ System.getProperty("line.separator")+"2";
		preferenceHandler.setScanner(new Scanner(in));
		assertEquals(1, preferenceHandler.fetchFrequencyIndex());
	}
	
	@Test	
	public void testCreateUserPreferences() throws Exception {
		String path = Paths.get("docs").toAbsolutePath().toString();
		String in = path
				+ System.getProperty("line.separator")
				+ path
				+ System.getProperty("line.separator")
				+"2";
		preferenceHandler.setScanner(new Scanner(in));
		UserBackupPreference preference = preferenceHandler.createUserPreferences();
//		assertEquals(path, preference.getMetadata().getDestinationFolder().toString());
		assertEquals(path, preference.getMetadata().getSourceFolder().toString());
		assertEquals(Frequencies.getSupportedFrequencies().get(1).getFrequencyName(), preference.getMetadata().getFrequencyName());
	}

}
