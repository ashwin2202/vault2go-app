package com.vault2go.monitor;

import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import org.apache.commons.io.monitor.FileAlterationObserver;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.vault2go.frequency.InstantBackup;
import com.vault2go.preferences.UserBackupPreference;

public class FileMonitorTest {
	
	FileMonitor fileMonitor;
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Mock
	private Map<String, FileAlterationObserver> observersMock;
	
	Path sourceFolder;
	
	UserBackupPreference backupPreference;

	@Before
	public void setUp() throws Exception {
		fileMonitor = FileMonitor.getInstance();
		sourceFolder = Paths.get("docs").toAbsolutePath();
		backupPreference=new UserBackupPreference(sourceFolder.toString(), new InstantBackup(), sourceFolder.toString());
	}

	@Test
	public void testAddNewObserver() throws Exception {
		assertTrue(fileMonitor.addNewObserver(backupPreference));
	}
	
	@Test
	public void testRemoveExistingObserver() throws Exception {
		fileMonitor.addNewObserver(backupPreference);
		assertTrue(fileMonitor.removeExistingObserver(backupPreference.getMetadata().getSourceFolder()));
	}

}
