package com.vault2go.monitor;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.monitor.FileAlterationObserver;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.vault2go.frequency.InstantBackup;
import com.vault2go.preferences.UserBackupPreference;

public class Vault2GoFileAlterationListenerTest {
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

	Vault2GoFileAlterationListener vault2GoFileAlterationListener;
	
	Path sourceFolder;
	
	Path destFolder;
	
	UserBackupPreference backupPreference;
	
	FileAlterationObserver fao;
	
	@Before
	public void setUp() throws Exception {
		sourceFolder = Paths.get("src/test/java/com/vault2go/monitor/test1").toAbsolutePath();
		destFolder = Paths.get("src/test/java/com/vault2go/monitor/test2").toAbsolutePath();
		backupPreference=new UserBackupPreference(sourceFolder.toString(), new InstantBackup(), destFolder.toString());
		fao = new FileAlterationObserver(backupPreference.getMetadata().getSourceFolder());
		vault2GoFileAlterationListener = new Vault2GoFileAlterationListener(backupPreference);
	}

	@Test
	public void testOnStart() {
		vault2GoFileAlterationListener.onStart(fao);
	}
	
	@Test
	public void testOnStop() {
		vault2GoFileAlterationListener.onStop(fao);
	}
	
	@Test
	public void testOnFileChange() {
		vault2GoFileAlterationListener.onFileChange(sourceFolder.toFile());
	}
	
	@Test
	public void testOnFileChange2() {
		Path sourceFolder2 = Paths.get("docs/sequence-diagram1.txt").toAbsolutePath();
		vault2GoFileAlterationListener.onFileChange(sourceFolder2.toFile());
	}
	
	@Test
	public void testOnFileCreate2() {
		Path sourceFolder2 = Paths.get("docs/sequence-diagram1.txt").toAbsolutePath();
		vault2GoFileAlterationListener.onFileCreate(sourceFolder2.toFile());
	}
	
	@Test
	public void testOnFileDelete() {
		vault2GoFileAlterationListener.onFileDelete(sourceFolder.toFile());
	}
	
	@Test
	public void testOnFileDelete2() {
		Path sourceFolder2 = Paths.get("docs/sequence-diagram1.txt").toAbsolutePath();
		vault2GoFileAlterationListener.onFileDelete(sourceFolder2.toFile());
	}
	
	@Test
	public void testOnDirChange() {
		vault2GoFileAlterationListener.onDirectoryChange(sourceFolder.toFile());
	}
	
	@Test
	public void testOnDirCreate() {
		vault2GoFileAlterationListener.onDirectoryCreate(sourceFolder.toFile());
	}
	
	@Test
	public void testOnDirDelete() {
		vault2GoFileAlterationListener.onDirectoryDelete(sourceFolder.toFile());
	}

}
