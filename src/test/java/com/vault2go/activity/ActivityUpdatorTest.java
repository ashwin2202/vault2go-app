package com.vault2go.activity;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.vault2go.db.helper.ActivityHelper;
import com.vault2go.event.ActivityUpdator;
import com.vault2go.event.FileCreationEvent;
import com.vault2go.event.FileDeletionEvent;
import com.vault2go.event.FileUpdateEvent;
import com.vault2go.service.Manager;

@SuppressWarnings("deprecation")
public class ActivityUpdatorTest {
	
Manager manager;
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Mock
	private ActivityHelper activityHelperMock;
	
	@Before
	public void setup() throws SQLException {
		when(activityHelperMock.saveActivity(any())).thenReturn(true);
	}

	@After
	public void tearDown() throws Exception {
		File dir = new File("logs");
		dir.delete();
	}

	@Test
	public void testWriteActivity_createEvent() throws IOException, SQLException {
		ActivityUpdator<FileCreationEvent> updator = new ActivityUpdator<FileCreationEvent>();
		updator.setActivityHelper(activityHelperMock);
		FileCreationEvent event=new FileCreationEvent("ActivityUpdatorTest.java", "/Users/aswinkumar/eclipse-workspace/vault2go/test/vault2go/activity/ActivityUpdatorTest.java");
		event.setTimestamp(new Date("Sat Jun 08 08:13:41 EDT 2019"));
		String result = updator.writeActivity(event);
		assertEquals(System.lineSeparator().toString()+"Sat Jun 08 08:13:41 EDT 2019 | 10001 | Path=/Users/aswinkumar/eclipse-workspace/vault2go/test/vault2go/activity/ActivityUpdatorTest.java, FileName=ActivityUpdatorTest.java", result);
	}
	
	@Test
	public void testWriteActivity_deleteEvent() throws IOException, SQLException {
		ActivityUpdator<FileDeletionEvent> updator = new ActivityUpdator<FileDeletionEvent>();
		updator.setActivityHelper(activityHelperMock);
		FileDeletionEvent event=new FileDeletionEvent("ActivityUpdatorTest.java", "/Users/aswinkumar/eclipse-workspace/vault2go/test/vault2go/activity/ActivityUpdatorTest.java");
		event.setTimestamp(new Date("Sat Jun 08 08:13:41 EDT 2019"));
		String result = updator.writeActivity(event);
		assertEquals(System.lineSeparator().toString()+"Sat Jun 08 08:13:41 EDT 2019 | 10003 | Path=/Users/aswinkumar/eclipse-workspace/vault2go/test/vault2go/activity/ActivityUpdatorTest.java, FileName=ActivityUpdatorTest.java", result);
	}
	
	@Test
	public void testWriteActivity_updateEvent() throws IOException, SQLException {
		ActivityUpdator<FileUpdateEvent> updator = new ActivityUpdator<FileUpdateEvent>();
		updator.setActivityHelper(activityHelperMock);
		FileUpdateEvent event=new FileUpdateEvent("ActivityUpdatorTest.java", "/Users/aswinkumar/eclipse-workspace/vault2go/test/vault2go/activity/ActivityUpdatorTest.java");
		event.setTimestamp(new Date("Sat Jun 08 08:13:41 EDT 2019"));
		String result = updator.writeActivity(event);
		assertEquals(System.lineSeparator().toString()+"Sat Jun 08 08:13:41 EDT 2019 | 10002 | Path=/Users/aswinkumar/eclipse-workspace/vault2go/test/vault2go/activity/ActivityUpdatorTest.java, FileName=ActivityUpdatorTest.java", result);
	}
	
	@Test
	public void testWriteActivityHandleEx() throws IOException, SQLException {
		ActivityUpdator<FileUpdateEvent> updator = new ActivityUpdator<FileUpdateEvent>();
		updator.setActivityHelper(activityHelperMock);
		FileUpdateEvent event=new FileUpdateEvent("ActivityUpdatorTest.java", "/Users/aswinkumar/eclipse-workspace/vault2go/test/vault2go/activity/ActivityUpdatorTest.java");
		event.setTimestamp(new Date("Sat Jun 08 08:13:41 EDT 2019"));
		String result = updator.writeActivity(event);
		assertEquals(System.lineSeparator().toString()+"Sat Jun 08 08:13:41 EDT 2019 | 10002 | Path=/Users/aswinkumar/eclipse-workspace/vault2go/test/vault2go/activity/ActivityUpdatorTest.java, FileName=ActivityUpdatorTest.java", result);
	}

}
