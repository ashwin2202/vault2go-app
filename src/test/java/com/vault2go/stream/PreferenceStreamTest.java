package com.vault2go.stream;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.vault2go.preferences.PreferenceMetadata;

public class PreferenceStreamTest {
	
	private PreferenceStream preferenceStream;

	@Before
	public void setUp() throws Exception {
		preferenceStream = PreferenceStream.getInstance();
	}

	@Test
	public void test() {
		preferenceStream.updatePreferences(new PreferenceMetadata("abcd", "/Users/aswinkumar/Documents/BU/CS622/back", "Instant", "/Users/aswinkumar/Documents/BU/CS622/back"));
		preferenceStream.updatePreferences(new PreferenceMetadata("abcd1234", "/Users/aswinkumar/Documents/BU/CS622/back", "Instant", "/Users/aswinkumar/Documents/BU/CS622/back"));
		assertEquals(2, preferenceStream.getPreferences().size());
	}

}
