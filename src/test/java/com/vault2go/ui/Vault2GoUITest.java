package com.vault2go.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.vault2go.service.Manager;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

public class Vault2GoUITest {
	
    private volatile boolean success = false;
    
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    Vault2GoUI goUI;
    
    @Before
	public void setUp() throws Exception {
    	goUI=new Vault2GoUI();
    	goUI.setManager(Manager.getInstance());
	}
    
    /**
     * Test that a JavaFX application launches.
     */
    @Test
    public void testMain() {
        Thread thread = new Thread() { // Wrapper thread.
            @Override
            public void run() {
                try {
                    Application.launch(Vault2GoUI.class); // Run JavaFX application.
                    success = true;
                } catch(Throwable t) {
                    if(t.getCause() != null && t.getCause().getClass().equals(InterruptedException.class)) {
                        // We expect to get this exception since we interrupted
                        // the JavaFX application.
                        success = true;
                        return;
                    }
                }
            }
        };
        thread.setDaemon(true);
        thread.start();
        try {
            Thread.sleep(3000);  // Wait for 3 seconds before interrupting JavaFX application
        } catch(InterruptedException ex) {
            // We don't care if we wake up early.
        }
        thread.interrupt();
        try {
            thread.join(1); // Wait 1 second for our wrapper thread to finish.
        } catch(InterruptedException ex) {
            // We don't care if we wake up early.
        }
        assertTrue(success);
    }
    
    @Test
	public void testHandler() throws Exception {
    	Vault2GoUI goUI=new Vault2GoUI();
    	Vault2GoUI.FrequencyOptionHandler frequencyOptionHandler=goUI.new FrequencyOptionHandler();
    	ComboBox<String> testCombo = new ComboBox<String>(FXCollections.observableArrayList());
    	testCombo.setValue("5");
    	testCombo.setOnAction(frequencyOptionHandler);
    	testCombo.fireEvent(new ActionEvent());
    	assertEquals(goUI.getFrequencyName(), "5");
	}
    
    @Test
	public void testHandler3() throws Exception {
    	Vault2GoUI.FrequencyOptionHandler frequencyOptionHandler=goUI.new FrequencyOptionHandler();
    	frequencyOptionHandler.handle(new ActionEvent());
	}
    
    @Test
	public void testHandler2() throws Exception {
    	Vault2GoUI.CreateBackupHandler createBackupHandler=goUI.new CreateBackupHandler();
    	goUI.setConfirmation(new Label());
    	goUI.setFrequencyName("Instant");
    	goUI.setSrc(Paths.get("docs").toString());
    	goUI.setDest(Paths.get("docs").toString());
    	createBackupHandler.handle(new ActionEvent());
	}
    
    @Test
	public void testHandler4() throws Exception {
    	Vault2GoUI.CreateBackupHandler createBackupHandler=goUI.new CreateBackupHandler();
    	goUI.setFrequencyName("Instant");
    	goUI.setSrc(Paths.get("docs").toString());
    	goUI.setDest(Paths.get("docs").toString());
    	goUI.setConfirmation(new Label());
    	createBackupHandler.handle(new ActionEvent());
	}
}
