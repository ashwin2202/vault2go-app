//package com.vault2go.service;
//
//import static org.mockito.Mockito.when;
//
//import java.io.IOException;
//import java.sql.SQLException;
//
//import org.junit.Before;
//import org.junit.Rule;
//import org.junit.Test;
//import org.mockito.Mock;
//import org.mockito.junit.MockitoJUnit;
//import org.mockito.junit.MockitoRule;
//
//public class WorkerTest {
//
//	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
//
//	@Mock
//	private Watcher watcherMock;
//
//	private Worker worker;
//
//	@Before
//	public void setUp() throws Exception {
//		worker = new Worker(watcherMock);
//		worker.setTestingEnabled(true);
//	}
//
//	@Test
//	public void testRun() {
//		worker.run();
//	}
//
//	@Test
//	public void testRun2() throws InterruptedException, IOException, SQLException {
//		when(watcherMock.processWatcherEvents()).thenThrow(new IOException());
//		worker.run();
//	}
//
//	@Test
//	public void testRun3() throws InterruptedException, IOException, SQLException {
//		when(watcherMock.processWatcherEvents()).thenThrow(new InterruptedException());
//		worker.run();
//	}
//
//}
