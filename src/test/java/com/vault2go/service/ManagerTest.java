package com.vault2go.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.vault2go.db.helper.UserPreferenceHelper;
import com.vault2go.preferences.PreferenceHandler;
import com.vault2go.preferences.PreferenceMetadata;
import com.vault2go.preferences.UserBackupPreference;

public class ManagerTest {
	
	Manager manager;
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Mock
	private PreferenceHandler preferenceHandlerMock;
	
	@Mock
	private UserBackupPreference userBackupPreferenceMock;
	
	@Mock
	private UserPreferenceHelper helperMock;

	@Before
	public void setUp() throws Exception {
		manager = Manager.getInstance();
		manager.setUserPreferenceHelper(helperMock);
		manager.setHandler(preferenceHandlerMock);
	}

//	@Test
//	public void testStartBackupLoop() throws IOException, SQLException {
//		assertTrue(manager.startBackupLoop());
//	}
	
	@Test
	public void testCreatePreferences() throws Exception {
		when(preferenceHandlerMock.createUserPreferences()).thenReturn(userBackupPreferenceMock);
		assertEquals(userBackupPreferenceMock, manager.createUserPreference());
	}
	
	@Test
	public void testResume() throws Exception {
		List<PreferenceMetadata> list = new ArrayList<PreferenceMetadata>();
		when(helperMock.getPreferencesDB()).thenReturn(list);
		assertEquals(true, manager.resumeBackup());
	}
	
	@Test
	public void testResume2() throws Exception {
		List<PreferenceMetadata> list = new ArrayList<PreferenceMetadata>();
		list.add(new PreferenceMetadata("1234", "docs", "InstantBackup", "b"));
		when(helperMock.getPreferencesDB()).thenReturn(list);
		assertEquals(true, manager.resumeBackup());
	}
	
	@Test
	public void testResume4() throws Exception {
		manager.setUserPreferenceHelper(null);
		List<PreferenceMetadata> list = new ArrayList<PreferenceMetadata>();
		list.add(new PreferenceMetadata("1234", "docs", "InstantBackup", "b"));
		when(helperMock.getPreferencesDB()).thenReturn(list);
		assertEquals(true, manager.resumeBackup());
	}
	
	@Test
	public void testResume3() throws Exception {
		when(helperMock.getPreferencesDB()).thenReturn(null);
		assertEquals(true, manager.resumeBackup());
	}

}
