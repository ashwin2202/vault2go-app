/**
 * 
 */
package com.vault2go.service;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.vault2go.db.helper.UserPreferenceHelper;
import com.vault2go.event.ActivityUpdator;
import com.vault2go.event.Event;
import com.vault2go.frequency.HourlyBackup;
import com.vault2go.frequency.InstantBackup;
import com.vault2go.preferences.UserBackupPreference;

/**
 * @author aswinkumar
 *
 */
public class WatcherTest {

	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Mock
	private ActivityUpdator<Event> activityUpdatorMock;

	@Mock
	private WatchService watchServiceMock;
	
	@Mock
	private WatchKey watchKeyMock;
	
	@Mock
	private UserPreferenceHelper helperMock;

	@SuppressWarnings("rawtypes")
	@Mock
	private WatchEvent watchEventMock;
	
	Watcher watcher;
	
	Path sourceFolder;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		watcher=new Watcher(100, TimeUnit.MILLISECONDS);
		watcher.setWatcherService(watchServiceMock);
		watcher.setUserPreferenceHelper(helperMock);
		sourceFolder = Paths.get("docs").toAbsolutePath();
		UserBackupPreference backupPreference=new UserBackupPreference(sourceFolder.toString(), new InstantBackup(), sourceFolder.toString());
		watcher.getRegisteredBackups().put(watchKeyMock, backupPreference);
		when(helperMock.saveUserPreference(any())).thenReturn(true);
	}

//	@Test
//	public void testProcessFileChangeEventsSuccess() throws InterruptedException, IOException, SQLException {
//		when(watchServiceMock.poll(watcher.getPollTime(), watcher.getUnits())).thenReturn(watchKeyMock);
//		when(watchEventMock.context()).thenReturn(sourceFolder);
//		when(watchEventMock.kind()).thenReturn(ENTRY_MODIFY);
//		List<WatchEvent<?>> eventsList = new ArrayList<WatchEvent<?>>();
//		eventsList.add(watchEventMock);
//		when(watchKeyMock.pollEvents()).thenReturn(eventsList);
//		when(watchKeyMock.reset()).thenReturn(true);
//		assertTrue(watcher.processWatcherEvents());
//	}
//	
//	@Test
//	public void testProcessFileChangeEventsSuccess2() throws InterruptedException, IOException, SQLException {
//		assertTrue(watcher.processWatcherEvents());
//	}
//	
//	@Test
//	public void testProcessFileChangeEventsSuccess3() throws InterruptedException, IOException, SQLException {
//		when(watchServiceMock.poll(watcher.getPollTime(), watcher.getUnits())).thenReturn(watchKeyMock);
//		when(watchEventMock.context()).thenReturn(sourceFolder);
//		when(watchEventMock.kind()).thenReturn(ENTRY_CREATE);
//		List<WatchEvent<?>> eventsList = new ArrayList<WatchEvent<?>>();
//		eventsList.add(watchEventMock);
//		when(watchKeyMock.pollEvents()).thenReturn(eventsList);
//		when(watchKeyMock.reset()).thenReturn(false);
//		assertTrue(watcher.processWatcherEvents());
//	}
//	
//	@Test
//	public void testhandleEventSuccess() throws InterruptedException, IOException, SQLException {
//		when(watchEventMock.context()).thenReturn(sourceFolder);
//		when(watchEventMock.kind()).thenReturn(ENTRY_CREATE);
//		assertTrue(watcher.handleEvent(watchKeyMock, watchEventMock));
//	}
//	
//	@Test
//	public void testhandleEventSuccess2() throws InterruptedException, IOException, SQLException {
//		when(watchEventMock.context()).thenReturn(sourceFolder);
//		when(watchEventMock.kind()).thenReturn(ENTRY_MODIFY);
//		assertTrue(watcher.handleEvent(watchKeyMock, watchEventMock));
//	}
//	
//	@Test
//	public void testhandleEventSuccess3() throws InterruptedException, IOException, SQLException {
//		when(watchEventMock.context()).thenReturn(sourceFolder);
//		when(watchEventMock.kind()).thenReturn(ENTRY_DELETE);
//		assertTrue(watcher.handleEvent(watchKeyMock, watchEventMock));
//	}
//	
//	@Test
//	public void testhandleEventFailure() throws InterruptedException, IOException, SQLException {
//		when(watchEventMock.context()).thenReturn(sourceFolder);
//		when(watchEventMock.kind()).thenReturn(ENTRY_DELETE);
//		assertTrue(watcher.handleEvent(watchKeyMock, watchEventMock));
//	}
//	
//	@Test(expected = IOException.class)
//	public void testhandleEventFailure2() throws IOException, SQLException{
//		when(fileUtilMock.findDirectoriesAndRegister(any(), any(), any())).thenThrow(new IOException());
//		when(watchEventMock.context()).thenReturn(sourceFolder);
//		when(watchEventMock.kind()).thenReturn(ENTRY_CREATE);
//		watcher.handleEvent(watchKeyMock, watchEventMock);
//	}
	
	@Test
	public void testAddBackupMonitorSuccess() throws Exception {
		UserBackupPreference backupPreference=new UserBackupPreference(sourceFolder.toString(), new HourlyBackup(), sourceFolder.toString());
		assertTrue(watcher.addBackupMonitor(backupPreference, false));
	}
}
