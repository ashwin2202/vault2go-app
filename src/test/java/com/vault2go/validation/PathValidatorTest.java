/**
 * 
 */
package com.vault2go.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.Test;

import com.vault2go.exception.ValidationException;

/**
 * @author aswinkumar
 *
 */
public class PathValidatorTest {

	private PathValidator validator;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		validator = new PathValidator();
	}

	@Test
	public void testSuccess() throws ValidationException {
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		assertTrue(validator.validatePath(Paths.get(s)));
	}

	@Test(expected = ValidationException.class)
	public void testFailure() throws ValidationException {
		try {
			validator.validatePath(Paths.get("/test"));
		}
		catch(ValidationException ex) {
			assertEquals("Invalid directory provided. Please try again.", ex.getMessage());
			assertEquals(3000, ex.getId());
			throw ex;
		}
	}

}
