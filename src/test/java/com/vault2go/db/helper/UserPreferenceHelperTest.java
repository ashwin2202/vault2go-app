package com.vault2go.db.helper;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.vault2go.db.connection.ConnectionManager;
import com.vault2go.frequency.Frequencies;
import com.vault2go.preferences.UserBackupPreference;

public class UserPreferenceHelperTest {
	
	UserPreferenceHelper helper;
	
	UserBackupPreference backupPreference;
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Before
	public void setUp() throws Exception {
		helper=new UserPreferenceHelper();
	}

	@Test
	public void testSaveActivity() throws SQLException, IOException {
		backupPreference=new UserBackupPreference("docs", Frequencies.getFrequencyByName("InstantBackup"), "docs");
		Connection jdbcConnection = Mockito.mock(Connection.class);
		helper.setConn(jdbcConnection);
		assertTrue(helper.saveUserPreference(backupPreference));
	}
	
	@Test
	public void testValidatePreferenceExists() throws SQLException, IOException {
		backupPreference=new UserBackupPreference("docs", Frequencies.getFrequencyByName("InstantBackup"), "docs");
		
		ResultSet resultSet = Mockito.mock(ResultSet.class);
		Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
		Mockito.when(resultSet.getString("backup_uuid")).thenReturn("abcdef");
		Mockito.when(resultSet.getString("source_location")).thenReturn("docs");
		Mockito.when(resultSet.getString("destination_location")).thenReturn("docs");
		Mockito.when(resultSet.getString("frequency_name")).thenReturn("InstantBackup");

		PreparedStatement statement = Mockito.mock(PreparedStatement.class);
		Mockito.when(statement.executeQuery()).thenReturn(resultSet);
		
		Statement createTableStatement = Mockito.mock(Statement.class);//
		Mockito.when(createTableStatement.execute(anyString())).thenReturn(true);

		Connection jdbcConnection = Mockito.mock(Connection.class);
		Mockito.when(jdbcConnection.prepareStatement(anyString())).thenReturn(statement);
		Mockito.when(jdbcConnection.createStatement()).thenReturn(createTableStatement);
		
		DataSource ds = Mockito.mock(DataSource.class);
		Mockito.when(ds.getConnection()).thenReturn(jdbcConnection);
		
		ConnectionManager.getInstance().setDataSource(ds);
		
		assertFalse(helper.validatePreferenceExists("InstantBackup", "docs2", "docs"));
	}
	
	@Test
	public void testValidatePreferenceExists2() throws SQLException, IOException {
		backupPreference=new UserBackupPreference("docs", Frequencies.getFrequencyByName("InstantBackup"), "docs");
		
		ResultSet resultSet = Mockito.mock(ResultSet.class);
		Mockito.when(resultSet.next()).thenReturn(false);

		PreparedStatement statement = Mockito.mock(PreparedStatement.class);
		Mockito.when(statement.executeQuery()).thenReturn(resultSet);

		Connection jdbcConnection = Mockito.mock(Connection.class);
		Mockito.when(jdbcConnection.prepareStatement(anyString())).thenReturn(statement);
		
		Statement createTableStatement = Mockito.mock(Statement.class);//
		Mockito.when(createTableStatement.execute(anyString())).thenReturn(true);
		Mockito.when(jdbcConnection.createStatement()).thenReturn(createTableStatement);
		
		DataSource ds = Mockito.mock(DataSource.class);
		Mockito.when(ds.getConnection()).thenReturn(jdbcConnection);
		
		ConnectionManager.getInstance().setDataSource(ds);
		
		assertTrue(helper.validatePreferenceExists("InstantBackup", "docs2", "docs"));
	}

}
