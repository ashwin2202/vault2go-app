package com.vault2go.db.helper;

import static org.junit.Assert.assertTrue;

import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.vault2go.event.FileCreationEvent;

public class ActivityHelperTest {
	
	ActivityHelper helper;
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

	@Before
	public void setUp() throws Exception {
		helper=new ActivityHelper();
	}

	@Test
	public void testSaveActivity() throws SQLException {
		Connection jdbcConnection = Mockito.mock(Connection.class);
		helper.setConn(jdbcConnection);
		assertTrue(helper.saveActivity(new FileCreationEvent("abc.java", Paths.get("docs").toString())));
	}

}
