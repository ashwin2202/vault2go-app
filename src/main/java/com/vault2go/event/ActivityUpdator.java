package com.vault2go.event;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.SQLException;

import com.vault2go.db.helper.ActivityHelper;
import com.vault2go.exception.ValidationException;
import com.vault2go.validation.PathValidator;

public class ActivityUpdator<T extends Event> implements Serializable{
	
	private ActivityHelper activityHelper;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6968986142680770515L;

	private PathValidator pathValidator=new PathValidator();

	private static final String ACTIVTY_FILENAME = "logs/history.log";

	public String writeActivity(T event) throws IOException, SQLException {
		try {
			pathValidator.validatePath(Paths.get(ACTIVTY_FILENAME));
		}
		catch(ValidationException ex) {
			File dir = new File("logs");
			dir.mkdirs();
			File tmp = new File(dir, "history.log");
			tmp.createNewFile();
		}

		StringBuilder lineItem = new StringBuilder();
		lineItem
			.append(System.lineSeparator().toString())
			.append(event.getTimestamp())
			.append(" | ")
			.append(event.getStatus())
			.append(" | ")
			.append(event.getDescription());
		Files.write(Paths.get(ACTIVTY_FILENAME), lineItem.toString().getBytes(), StandardOpenOption.APPEND);
		System.out.println("Written event to activity log file");
		//write event to activity table as well
		if(getActivityHelper()==null) {
			setActivityHelper(new ActivityHelper());
		}
		getActivityHelper().saveActivity(event);
		System.out.println("Written event to activity history table");
		return lineItem.toString();
	}

	/**
	 * @return the activityHelper
	 */
	public ActivityHelper getActivityHelper() {
		return activityHelper;
	}

	/**
	 * @param activityHelper the activityHelper to set
	 */
	public void setActivityHelper(ActivityHelper activityHelper) {
		this.activityHelper = activityHelper;
	}
}
