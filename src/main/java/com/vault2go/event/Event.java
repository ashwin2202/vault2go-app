package com.vault2go.event;

import java.util.Date;

import com.vault2go.event.EventType.EventTypes;

public abstract class Event {

	private Date timestamp;

	private String description;

	private int status;
	
	private EventTypes type;
	
	/**
	 * @param timestamp
	 * @param description
	 * @param status
	 * @param type
	 */
	public Event(Date timestamp, String description, int status, EventTypes type) {
		super();
		this.timestamp = timestamp;
		this.description = description;
		this.status = status;
		this.type = type;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the type
	 */
	public EventTypes getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(EventTypes type) {
		this.type = type;
	}
	
	

}
