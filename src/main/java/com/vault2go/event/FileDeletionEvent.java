package com.vault2go.event;

import java.util.Date;

import com.vault2go.event.EventType.EventTypes;

public class FileDeletionEvent extends Event {

	public FileDeletionEvent(String fileName, String path) {
		super(new Date(), "Path="+path+", FileName="+fileName, 10003, EventTypes.FILE_DELETED);
	}
	
}
