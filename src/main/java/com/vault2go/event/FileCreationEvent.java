package com.vault2go.event;

import java.util.Date;

import com.vault2go.event.EventType.EventTypes;

public class FileCreationEvent extends Event {

	public FileCreationEvent(String fileName, String path) {
		super(new Date(), "Path="+path+", FileName="+fileName, 10001, EventTypes.FILE_CREATED);
	}
	
}
