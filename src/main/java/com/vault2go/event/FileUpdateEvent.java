package com.vault2go.event;

import java.util.Date;

import com.vault2go.event.EventType.EventTypes;

public class FileUpdateEvent extends Event {

	public FileUpdateEvent(String fileName, String path) {
		super(new Date(), "Path="+path+", FileName="+fileName, 10002, EventTypes.FILE_MODIFIED);
	}
	
}
