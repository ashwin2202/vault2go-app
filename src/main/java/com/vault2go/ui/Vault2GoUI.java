/**
 * 
 */
package com.vault2go.ui;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import com.vault2go.db.helper.UserPreferenceHelper;
import com.vault2go.frequency.BackupFrequency;
import com.vault2go.frequency.Frequencies;
import com.vault2go.preferences.UserBackupPreference;
import com.vault2go.service.Manager;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

/**
 * @author aswinkumar
 *
 */
public class Vault2GoUI extends Application{

	private String src;

	private String dest;

	private String frequencyName;

	private Manager manager;

	private Label confirmation;
	
	private UserPreferenceHelper userPreferenceHelper;

	@Override
	public void start(Stage primaryStage) throws Exception {
		StackPane pane = new StackPane();

		primaryStage.setTitle("Vault2Go Backup Solutions");
		Scene scene = new Scene(new Group(), 600, 400);

		//Set title
		Label title = new Label ("Vault2Go Backup Solutions");
		title.setFont(new Font("Cambria", 30));

		//Backup button
		Button firstButton = new Button("Get Started!");
		CreateBackupHandler createBackupHandler = new CreateBackupHandler();
		firstButton.setOnAction(createBackupHandler);
		pane.getChildren().add(firstButton);

		TitledPane gridTitlePane = new TitledPane();
		gridTitlePane.setCollapsible(false);
		gridTitlePane.setAnimated(false);

		GridPane grid = new GridPane();
		grid.setVgap(4);
		grid.setPadding(new Insets(5, 5, 5, 5));

		//Set source
		Label sourceFolderLbl = new Label ("Directory to be backed up");
		sourceFolderLbl.setFont(new Font("Cambria", 15));
		TextField sourceFolderTxt = new TextField();
		sourceFolderTxt.setMinSize(200, 10);
		sourceFolderTxt.setPromptText("source directory");
		final Button browseButtonSrc = new Button("...");
		browseButtonSrc.setOnAction(
				(final ActionEvent e) -> {
					final DirectoryChooser directoryChooser=new DirectoryChooser();
					final File selectedDirectory =
							directoryChooser.showDialog(primaryStage);
					if (selectedDirectory != null) {
						src = selectedDirectory.getAbsolutePath();
						sourceFolderTxt.setText(src);
					}
				});

		//Set source
		Label destFolder = new Label ("Directory where backup needs to be stored   ");
		destFolder.setFont(new Font("Cambria", 15));
		TextField destFolderTxt = new TextField();
		destFolderTxt.setMinSize(200, 10);
		destFolderTxt.setPromptText("target directory");
		final Button browseButtonDest = new Button("...");
		browseButtonDest.setOnAction(
				(final ActionEvent e) -> {
					final DirectoryChooser directoryChooser=new DirectoryChooser();
					final File selectedDirectory =
							directoryChooser.showDialog(primaryStage);
					if (selectedDirectory != null) {
						dest = selectedDirectory.getAbsolutePath();
						destFolderTxt.setText(dest);
					}
				});

		//Set frequency
		Label frequency = new Label ("Backup Frequency");
		frequency.setFont(new Font("Cambria", 15));
		manager = Manager.getInstance();
		ObservableList<String> options = FXCollections.observableArrayList();
		for(BackupFrequency frequencyOpt: Frequencies.getSupportedFrequencies()) {
			options.add(frequencyOpt.getUserFriendlyFrequencyName());
		}
		final ComboBox<String> comboBox = new ComboBox<String>(options);
		FrequencyOptionHandler frequencyOptionHandler=new FrequencyOptionHandler();
		comboBox.setOnAction(frequencyOptionHandler);

		grid.add(sourceFolderLbl, 0, 0);
		grid.add(sourceFolderTxt, 1, 0);
		grid.add(browseButtonSrc, 2, 0);
		grid.add(destFolder, 0, 1);
		grid.add(destFolderTxt, 1, 1);
		grid.add(browseButtonDest, 2, 1);
		grid.add(frequency, 0, 2);
		grid.add(comboBox, 1, 2);        
		gridTitlePane.setText("Preferences");
		gridTitlePane.setContent(grid);
		gridTitlePane.setMinSize(300, 200);

		HBox hbox = new HBox(10);
		hbox.setPadding(new Insets(40, 0, 0, 40));
		hbox.getChildren().setAll(gridTitlePane);

		//Set title
		confirmation = new Label();
		title.setFont(new Font("Cambria", 15));
		confirmation.setVisible(false);
		confirmation.setPadding(new Insets(5, 0, 0, 5));

		VBox vbox = new VBox(10);
		vbox.setAlignment(Pos.BASELINE_CENTER);
		vbox.getChildren().setAll(title, hbox, firstButton,confirmation);

		Group root = (Group)scene.getRoot();
		root.getChildren().add(vbox);
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	//Class which would handle the create backup button action and creates user backup
	class CreateBackupHandler implements EventHandler<ActionEvent>{

		public void handle(ActionEvent event){
			System.out.println("Inside create backup");
			System.out.println("src >>> "+src);
			System.out.println("dest >>> "+dest);
			System.out.println("frequency >>> "+frequencyName);
			BackupFrequency selectedFrequency = null;
			try {
				for(BackupFrequency frequencyOpt: Frequencies.getSupportedFrequencies()) {
					if(frequencyName.equalsIgnoreCase(frequencyOpt.getUserFriendlyFrequencyName())) {
						selectedFrequency = frequencyOpt;
					}
				}
			} catch (IOException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			UserBackupPreference backupPreference = new UserBackupPreference(src, selectedFrequency, dest);
			System.out.println(backupPreference);
			try {
				userPreferenceHelper = new UserPreferenceHelper();
				if(userPreferenceHelper.validatePreferenceExists(backupPreference.getMetadata().getFrequencyName(),
						backupPreference.getMetadata().getSourceFolder(),
						backupPreference.getMetadata().getDestinationFolder())) {
					selectedFrequency.getWatcherService().addBackupMonitor(backupPreference, false);
					confirmation.setText("Backup Created successfully: "+backupPreference.getMetadata().getId());
					confirmation.setVisible(true);
				}
				else {
					confirmation.setText("Sorry backup already exists with similar selection. Please try a different selection");
					confirmation.setVisible(true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class FrequencyOptionHandler implements EventHandler<ActionEvent>{
		@SuppressWarnings("unchecked")
		public void handle(ActionEvent event){
			if(event.getSource() instanceof ComboBox) {
				frequencyName = ((ComboBox<String>)event.getSource()).getValue();
			}
		}
	}

	/**
	 * @return the frequencyName
	 */
	public String getFrequencyName() {
		return frequencyName;
	}

	/**
	 * @param frequencyName the frequencyName to set
	 */
	public void setFrequencyName(String frequencyName) {
		this.frequencyName = frequencyName;
	}

	/**
	 * @return the manager
	 */
	public Manager getManager() {
		return manager;
	}

	/**
	 * @param manager the manager to set
	 */
	public void setManager(Manager manager) {
		this.manager = manager;
	}

	/**
	 * @return the src
	 */
	public String getSrc() {
		return src;
	}

	/**
	 * @param src the src to set
	 */
	public void setSrc(String src) {
		this.src = src;
	}

	/**
	 * @return the dest
	 */
	public String getDest() {
		return dest;
	}

	/**
	 * @param dest the dest to set
	 */
	public void setDest(String dest) {
		this.dest = dest;
	}

	/**
	 * @return the confirmation
	 */
	public Label getConfirmation() {
		return confirmation;
	}

	/**
	 * @param confirmation the confirmation to set
	 */
	public void setConfirmation(Label confirmation) {
		this.confirmation = confirmation;
	}

}

