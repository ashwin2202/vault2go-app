package com.vault2go.validation;

import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import com.vault2go.exception.ValidationException;

public class PathValidator implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4807560201398339166L;

	public boolean validatePath(Path dir) throws ValidationException {
		if(Files.exists(dir, LinkOption.NOFOLLOW_LINKS)) {
			return true;
		}
		else {
			throw new ValidationException(3000, "Invalid directory provided. Please try again.");
		}
	}

}
