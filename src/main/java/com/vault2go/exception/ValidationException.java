package com.vault2go.exception;

public class ValidationException extends Throwable {
	
	private int id;
	
	private String message;

	/**
	 * 
	 */
	private static final long serialVersionUID = -4426920563315263807L;

	/**
	 * @param id
	 * @param message
	 */
	public ValidationException(int id, String message) {
		super();
		this.id = id;
		this.message = message;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
}
