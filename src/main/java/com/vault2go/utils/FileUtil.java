///**
// * 
// */
//package com.vault2go.utils;
//
//import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
//import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
//import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
//
//import java.io.IOException;
//import java.io.Serializable;
//import java.nio.file.FileVisitResult;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.nio.file.SimpleFileVisitor;
//import java.nio.file.WatchKey;
//import java.nio.file.WatchService;
//import java.nio.file.attribute.BasicFileAttributes;
//import java.util.Map;
//
//import com.vault2go.preferences.UserBackupPreference;
//
///**
// * @author aswinkumar
// *
// */
//public class FileUtil implements Serializable{
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 7185888606324397616L;
//
//	public boolean findDirectoriesAndRegister(WatchService service, UserBackupPreference userBackupPreference, Map<WatchKey, UserBackupPreference> backups) throws IOException {
//		Files.walkFileTree(Paths.get(userBackupPreference.getMetadata().getSourceFolder()), new SimpleFileVisitor<Path>() {
//			@Override
//			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
//				backups.put(registerBackupDirectory(service, userBackupPreference), userBackupPreference);
//				String concatenatedPath = dir.toString().replace(userBackupPreference.getMetadata().getSourceFolder().toString(), "");
//				System.out.println("Number of files copied from "+dir+" :: "+userBackupPreference.getProvider().copyFiles(dir, Paths.get(userBackupPreference.getMetadata().getDestinationFolder().toString()+concatenatedPath)));
//				return FileVisitResult.CONTINUE;
//			}
//		});
//		return true;
//	}
//	
//	public WatchKey registerBackupDirectory(WatchService service, UserBackupPreference userBackupPreference) throws IOException {
//		return Paths.get(userBackupPreference.getMetadata().getSourceFolder()).register(service, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
//	}
//	
//}
