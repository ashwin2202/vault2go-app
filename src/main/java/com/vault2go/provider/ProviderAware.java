package com.vault2go.provider;

import java.io.IOException;

public interface ProviderAware {

	public boolean copyAFile(String source, String destination) throws IOException;
	
	public boolean copyADirectory(String source, String destination) throws IOException;

}
