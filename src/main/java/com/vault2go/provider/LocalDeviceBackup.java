/**
 * 
 */
package com.vault2go.provider;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;

import com.vault2go.concurrency.LocalFileCopier;

/**
 * @author aswinkumar
 *
 */
public class LocalDeviceBackup implements ProviderAware, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6892162805436290639L;

	@Override
	public boolean copyAFile(String src, String dest) throws IOException {
		boolean status = false;
		try {
			LocalFileCopier.getInstance().makeACopy(src, dest);
			status=true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	public boolean copyADirectory(String source, String destination) throws IOException {
		return true;
	}

}
