package com.vault2go.stream;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.vault2go.preferences.PreferenceMetadata;

public class PreferenceStream {
	
	private static PreferenceStream instance;
	
	public static synchronized PreferenceStream getInstance() throws IOException{
		if(instance == null){
			instance = new PreferenceStream();
		}
		return instance;
	}
	
	private ObjectOutputStream outfile;
	
	private ObjectInputStream infile;
	
	private PreferenceStream() {
		outfile = getOutputStream();
		infile = getInputStream();
	}

	private static final String PREFERENCE_FILENAME = "local-preferences.dat";

	public boolean updatePreferences(PreferenceMetadata metadata) {
		List<PreferenceMetadata> mList = getPreferences();
		if(mList==null) {
			mList = new ArrayList<PreferenceMetadata>();
		}
		mList.add(metadata);
		try {
			outfile.writeObject(mList);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<PreferenceMetadata> getPreferences(){
		List<PreferenceMetadata> metadataList = null;
		try {
			while (true){
				metadataList = (ArrayList<PreferenceMetadata>) infile.readObject();
			}
		} catch (EOFException e) {
			System.out.println("End of preference file has reached.");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return metadataList;
	}

	private ObjectOutputStream getOutputStream() {
		try{
			File tmp = null;
			if(Files.notExists(Paths.get(PREFERENCE_FILENAME))) {
				tmp = new File(PREFERENCE_FILENAME);
				tmp.createNewFile();
			}
			outfile = new ObjectOutputStream(new FileOutputStream(PREFERENCE_FILENAME));
		}
		catch (IOException ex){
			System.out.println("IOException");
			ex.printStackTrace();
		}
		return outfile;
	}

	private ObjectInputStream getInputStream() {
		try{
			File tmp = null;
			if(Files.notExists(Paths.get(PREFERENCE_FILENAME))) {
				tmp = new File(PREFERENCE_FILENAME);
				tmp.createNewFile();
			}
			infile = new ObjectInputStream(new FileInputStream(PREFERENCE_FILENAME));
		}
		catch (IOException ex){
			System.out.println("IOException");
			ex.printStackTrace();
		}
		return infile;
	}

}
