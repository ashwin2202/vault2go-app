package com.vault2go.concurrency;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;

public class LocalFileCopier {

	private static LocalFileCopier instance;

	private ExecutorService threadPoolExecutor;

	private int corePoolSize  = 10;

	private int maxPoolSize = 20;

	private long keepAliveTime = 5000;
	
	public LocalFileCopier() {
		threadPoolExecutor =
				new ThreadPoolExecutor(
						corePoolSize,
						maxPoolSize,
						keepAliveTime,
						TimeUnit.MILLISECONDS,
						new LinkedBlockingQueue<Runnable>()
						);
		//stop monitoring files
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
        	
            @Override
            public void run() {
                try {
                    System.out.println("Stopping monitor.");
                    threadPoolExecutor.shutdown();
                } catch (Exception ignored) {
                }
            }
        }));
	}

	public static synchronized LocalFileCopier getInstance() throws IOException, SQLException{
		if(instance == null){
			instance = new LocalFileCopier();
		}
		return instance;
	}

	public static void main(String[] args) throws IOException, SQLException {
		LocalFileCopier.getInstance().makeACopy(Paths.get("docs/vault2go-app.gif").toString(), Paths.get("logs/vault2go-app.gif").toString());
	}

	public void makeACopy(String src, String dest) {
		Runnable r=new LocalCopyWorker(src, dest);
		threadPoolExecutor.execute(r);
	}

}

class LocalCopyWorker implements Runnable {

	private String src;

	private String dest;

	public LocalCopyWorker(String src, String dest) {
		this.src = src;
		this.dest = dest;
	}

	@Override
	public void run() {
		boolean status = false;
		try {
			if(src!=null && dest!=null) {
				FileUtils.copyFile(new File(src), new File(dest));
			}
			status=true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.format(Thread.currentThread().getName()+" :: Copy job completed - status: %s, src: %s, dest= %s\n", status, src, dest);
	}

}
