/**
 * 
 */
package com.vault2go.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import com.vault2go.db.helper.UserPreferenceHelper;
import com.vault2go.frequency.Frequencies;
import com.vault2go.monitor.FileMonitor;
import com.vault2go.preferences.PreferenceHandler;
import com.vault2go.preferences.PreferenceMetadata;
import com.vault2go.preferences.UserBackupPreference;

/**
 * @author aswinkumar
 *
 */
public class Manager {

	private static Manager instance;

	private Manager() throws IOException, SQLException{
		handler=new PreferenceHandler(Frequencies.getSupportedFrequencies());
	}

	public static synchronized Manager getInstance() throws IOException, SQLException{
		if(instance == null){
			instance = new Manager();
		}
		return instance;
	}

	private PreferenceHandler handler;
	
	private UserPreferenceHelper userPreferenceHelper;

//	public boolean startBackupLoop() throws IOException, SQLException {
//		ExecutorService executor = Executors.newFixedThreadPool(Frequencies.getSupportedFrequencies().size());
//		for(BackupFrequency frequency: Frequencies.getSupportedFrequencies()) {
//			Runnable worker = new Worker(frequency.getWatcherService());
//			executor.execute(worker);
//		}
//		return true;
//	}
	
	public boolean resumeBackup() throws Exception {
		if(getUserPreferenceHelper()==null) {
			setUserPreferenceHelper(new UserPreferenceHelper());
		}
		List<PreferenceMetadata> list=getUserPreferenceHelper().getPreferencesDB();
		if(list!=null) {
			System.out.println("list size ::::"+list.size());
			for(PreferenceMetadata metadata: list) {
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>"+metadata.getFrequencyName());
				System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>"+Frequencies.getFrequencyByName(metadata.getFrequencyName()));
				UserBackupPreference backupPreference=new UserBackupPreference(metadata.getSourceFolder(), Frequencies.getFrequencyByName(metadata.getFrequencyName()), metadata.getDestinationFolder());
				//frequencyOpt.getWatcherService().addBackupMonitor(backupPreference, true);
				FileMonitor.getInstance().addNewObserver(backupPreference);
			}
		}
		return true;
	}

	public UserBackupPreference createUserPreference() throws Exception {
		return getHandler().createUserPreferences();
	}

	/**
	 * @return the handler
	 */
	public PreferenceHandler getHandler() {
		return handler;
	}

	/**
	 * @param handler the handler to set
	 */
	public void setHandler(PreferenceHandler handler) {
		this.handler = handler;
	}

	/**
	 * @return the userPreferenceHelper
	 */
	public UserPreferenceHelper getUserPreferenceHelper() {
		return userPreferenceHelper;
	}

	/**
	 * @param userPreferenceHelper the userPreferenceHelper to set
	 */
	public void setUserPreferenceHelper(UserPreferenceHelper userPreferenceHelper) {
		this.userPreferenceHelper = userPreferenceHelper;
	}

}