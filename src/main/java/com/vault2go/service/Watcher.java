/**
 * 
 */
package com.vault2go.service;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.vault2go.db.helper.UserPreferenceHelper;
import com.vault2go.event.ActivityUpdator;
import com.vault2go.event.Event;
import com.vault2go.monitor.FileMonitor;
import com.vault2go.preferences.UserBackupPreference;

/**
 * @author aswinkumar
 *
 */
public class Watcher implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -515199460408492866L;

	private WatchService watcherService;

	private ActivityUpdator<Event> activityUpdator;

	private Map<WatchKey, UserBackupPreference> registeredBackups;

	private int pollTime;

	private TimeUnit units;

	private UserPreferenceHelper userPreferenceHelper;

	public Watcher(int pollTime, TimeUnit units) throws IOException, SQLException {
		this.watcherService = FileSystems.getDefault().newWatchService();
		this.registeredBackups = new HashMap<WatchKey, UserBackupPreference>();
		this.pollTime = pollTime;
		this.units = units;
		setActivityUpdator(new ActivityUpdator<Event>());
		userPreferenceHelper = new UserPreferenceHelper();
	}

	public boolean addBackupMonitor(UserBackupPreference preference, boolean metadataExists) throws Exception {
		File srcDir = new File(preference.getMetadata().getSourceFolder());
		//		FileUtils.copyDirectoryToDirectory(srcDir,new File(preference.getMetadata().getDestinationFolder()));
		preference.getMetadata().setDestinationFolder(preference.getMetadata().getDestinationFolder()+"/"+srcDir);
		FileMonitor.getInstance().addNewObserver(preference);
		//getFileUtil().findDirectoriesAndRegister(getWatcherService(), preference, registeredBackups);
		System.out.println("Copy completed...");
		if(!metadataExists) {
			//preferencesStream.updatePreferences(preference.getMetadata());

			//remove file storage and persist the preference in preference table in DB
			if(getUserPreferenceHelper()==null) {
				setUserPreferenceHelper(new UserPreferenceHelper());
			}
			getUserPreferenceHelper().saveUserPreference(preference);
		}
		return true;
	}

	//	public boolean processWatcherEvents() throws InterruptedException, IOException, SQLException {
	//		//System.out.println("inside processWatcherEvents... polltime "+getPollTime()+getUnits());
	//		// wait for key to be signalled
	//		WatchKey key = getWatcherService().poll(getPollTime(), getUnits());
	//		if(key!=null) {
	//			System.out.println("inside processWatcherEvents... polltime "+getPollTime()+getUnits());
	//			for (WatchEvent<?> event : key.pollEvents()) {
	//				handleEvent(key, event);
	//			}
	//			// reset key and remove from set if directory no longer accessible
	//			boolean valid = key.reset();
	//			if (!valid) {
	//				registeredBackups.remove(key);
	//			}
	//		}	
	//		return true;
	//	}

	//	@SuppressWarnings("rawtypes")
	//	public boolean handleEvent(WatchKey key, WatchEvent event) throws IOException, SQLException {
	//		WatchEvent.Kind kind = event.kind();
	//		// Context for directory entry event is the file name of entry
	//		@SuppressWarnings("unchecked")
	//		Path name = ((WatchEvent<Path>)event).context();
	//		
	//		UserBackupPreference selectedPreference = registeredBackups.get(key);
	//		Path child = Paths.get(selectedPreference.getMetadata().getSourceFolder()).resolve(name);
	//		System.out.format(Thread.currentThread().getName()+" - %s: %s\n", event.kind().name(), child);
	//		Event logEvent=null;
	//		if (kind == ENTRY_CREATE) {
	//			logEvent = new FileCreationEvent(name.toString(), child.toString());
	//			try {
	//				if (Files.isDirectory(child)) {
	//					getFileUtil().findDirectoriesAndRegister(getWatcherService(), selectedPreference, registeredBackups);
	//				}
	//				else {
	//					copyFilesToTarget(selectedPreference, child);
	//				}
	//			} catch (IOException x) {
	//				x.printStackTrace();
	//				throw x;
	//			}
	//		}
	//		else if(kind == ENTRY_MODIFY) {
	//			copyFilesToTarget(selectedPreference, child);
	//			logEvent = new FileUpdateEvent(name.toString(), child.toString());
	//		}
	//		else {
	//			logEvent = new FileDeletionEvent(name.toString(), child.toString());
	//		}
	//		getActivityUpdator().writeActivity(logEvent);
	//		return true;
	//	}
	//	
	//	private void copyFilesToTarget(UserBackupPreference selectedPreference, Path child) throws IOException {
	//		String tmpSrc = child.toString().replace(selectedPreference.getMetadata().getSourceFolder().toString(), "");
	//		int count = selectedPreference.getProvider().copyFiles(child, Paths.get(selectedPreference.getMetadata().getDestinationFolder().toString()+tmpSrc));
	//		System.out.println("Number of files copied from "+child+" :: "+count);
	//	}

	/**
	 * @return the watcherAPI
	 */
	public WatchService getWatcherService() {
		return watcherService;
	}

	/**
	 * @return the pollTime
	 */
	public int getPollTime() {
		return pollTime;
	}

	/**
	 * @return the units
	 */
	public TimeUnit getUnits() {
		return units;
	}

	/**
	 * @return the activityUpdator
	 */
	public ActivityUpdator<Event> getActivityUpdator() {
		return activityUpdator;
	}

	/**
	 * @param watcherService the watcherService to set
	 */
	public void setWatcherService(WatchService watcherService) {
		this.watcherService = watcherService;
	}

	/**
	 * @param activityUpdator the activityUpdator to set
	 */
	public void setActivityUpdator(ActivityUpdator<Event> activityUpdator) {
		this.activityUpdator = activityUpdator;
	}

	/**
	 * @return the registeredBackups
	 */
	public Map<WatchKey, UserBackupPreference> getRegisteredBackups() {
		return registeredBackups;
	}

	/**
	 * @return the userPreferenceHelper
	 */
	public UserPreferenceHelper getUserPreferenceHelper() {
		return userPreferenceHelper;
	}

	/**
	 * @param userPreferenceHelper the userPreferenceHelper to set
	 */
	public void setUserPreferenceHelper(UserPreferenceHelper userPreferenceHelper) {
		this.userPreferenceHelper = userPreferenceHelper;
	}

}
