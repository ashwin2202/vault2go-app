//package com.vault2go.service;
//
//import java.io.IOException;
//import java.sql.SQLException;
//
//public class Worker implements Runnable {
//	
//	private Watcher watcher;
//	
//	private boolean testingEnabled;
//	
//	/**
//	 * @param watcher
//	 */
//	public Worker(Watcher watcher) {
//		super();
//		this.watcher = watcher;
//		this.testingEnabled = false;
//	}
//
//	@Override
//	public void run() {
//		System.out.println("Testing enabled flag > "+testingEnabled);
//		try {
//			do {
//				//System.out.println(Thread.currentThread().getName()+" :::: Inside run, while loop.... ");
//				getWatcher().processWatcherEvents();
//			} while (!isTestingEnabled());
//		} catch (InterruptedException | IOException | SQLException e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * @return the watcher
//	 */
//	public Watcher getWatcher() {
//		return watcher;
//	}
//
//	/**
//	 * @return the testingEnabled
//	 */
//	public boolean isTestingEnabled() {
//		return testingEnabled;
//	}
//
//	/**
//	 * @param testingEnabled the testingEnabled to set
//	 */
//	public void setTestingEnabled(boolean testingEnabled) {
//		this.testingEnabled = testingEnabled;
//	}
//
//}
