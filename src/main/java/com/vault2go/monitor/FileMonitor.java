package com.vault2go.monitor;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;

import com.vault2go.preferences.UserBackupPreference;

public class FileMonitor {
	
	private static FileMonitor instance;
	
	private FileAlterationMonitor monitor;
	
	private Map<String, FileAlterationObserver> observersMap;
	
	public FileMonitor() throws Exception {
		setObserversMap(new HashMap<String, FileAlterationObserver>());
		setMonitor(new FileAlterationMonitor());
		getMonitor().start();

		//stop monitoring files
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
        	
            @Override
            public void run() {
                try {
                    System.out.println("Stopping monitor.");
                    getMonitor().stop();
                } catch (Exception ignored) {
                }
            }
        }));
	}

	public static synchronized FileMonitor getInstance() throws Exception{
		if(instance == null){
			instance = new FileMonitor();
		}
		return instance;
	}

    public boolean addNewObserver(UserBackupPreference backupPreference) throws Exception {
    	FileAlterationObserver fao = new FileAlterationObserver(backupPreference.getMetadata().getSourceFolder());
        fao.addListener(new Vault2GoFileAlterationListener(backupPreference));
        getMonitor().addObserver(fao);
        getObserversMap().put(backupPreference.getMetadata().getSourceFolder(), fao);
    	return true;
    }
    
    public boolean removeExistingObserver(String src) throws Exception {
    	getMonitor().removeObserver(getObserversMap().get(src));
    	getObserversMap().remove(src);
		return true;
    }

	/**
	 * @return the monitor
	 */
	public FileAlterationMonitor getMonitor() {
		return monitor;
	}

	/**
	 * @param monitor the monitor to set
	 */
	public void setMonitor(FileAlterationMonitor monitor) {
		this.monitor = monitor;
	}

	/**
	 * @return the observersMap
	 */
	public Map<String, FileAlterationObserver> getObserversMap() {
		return observersMap;
	}

	/**
	 * @param observersMap the observersMap to set
	 */
	public void setObserversMap(Map<String, FileAlterationObserver> observersMap) {
		this.observersMap = observersMap;
	}
}