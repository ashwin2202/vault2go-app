package com.vault2go.monitor;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;

import com.vault2go.frequency.Frequencies;
import com.vault2go.preferences.UserBackupPreference;

public class Vault2GoFileAlterationListener implements FileAlterationListener {

	private UserBackupPreference backupPreference;

	public Vault2GoFileAlterationListener(UserBackupPreference backupPreferenceIn) {
		this.backupPreference = backupPreferenceIn;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onStart(final FileAlterationObserver observer) {
		System.out.println(Thread.currentThread().getName()+" :: "+"FileListener has started on " + observer.getDirectory().getAbsolutePath());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDirectoryCreate(final File directory) {
		System.out.println(Thread.currentThread().getName()+" :: "+directory.getAbsolutePath() + " was created.");
		UserBackupPreference backupPreference2;
		try {
			String tmpPath = directory.getAbsolutePath().replace(backupPreference.getMetadata().getSourceFolder(), "");
			FileUtils.forceMkdir(new File(backupPreference.getMetadata().getDestinationFolder().concat(tmpPath)));
			backupPreference2 = new UserBackupPreference(directory.getAbsolutePath(), 
					Frequencies.getFrequencyByName(backupPreference.getMetadata().getFrequencyName()), 
					backupPreference.getMetadata().getDestinationFolder().concat(tmpPath));
			FileMonitor.getInstance().addNewObserver(backupPreference2);
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDirectoryChange(final File directory) {
		System.out.println(Thread.currentThread().getName()+" :: "+directory.getAbsolutePath() + " was modified");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onDirectoryDelete(final File directory) {
		System.out.println(Thread.currentThread().getName()+" :: "+directory.getAbsolutePath() + " was deleted.");
		try {
			FileMonitor.getInstance().removeExistingObserver(directory.getAbsolutePath());
			FileUtils.deleteDirectory(new File(directory.getAbsolutePath()));
			System.out.println("Monitor was removed for >>> "+directory.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onFileCreate(final File file) {
		//        System.out.println(file.getAbsoluteFile() + " was created.");
		//        System.out.println(Thread.currentThread().getName()+" :: "+">>>------> length: " + file.length());
		//        System.out.println(Thread.currentThread().getName()+" :: "+">>>------> last modified: " + new Date(file.lastModified()));
		//        System.out.println(Thread.currentThread().getName()+" :: "+">>>------> readable: " + file.canRead());
		//        System.out.println(Thread.currentThread().getName()+" :: "+">>>------> writable: " + file.canWrite());
		//        System.out.println(Thread.currentThread().getName()+" :: "+">>>------> executable: " + file.canExecute());
		try {
			String tmpPath = file.getAbsolutePath().replace(backupPreference.getMetadata().getSourceFolder(), "");
			backupPreference.getProvider().copyAFile(file.getAbsolutePath(), backupPreference.getMetadata().getDestinationFolder()+tmpPath);
			//FileUtils.copyFile(new File(file.getAbsolutePath()), new File(backupPreference.getMetadata().getDestinationFolder()+tmpPath), true);
			//backupPreference.getProvider().copyFiles(Paths.get(file.getAbsolutePath()), Paths.get(backupPreference.getMetadata().getDestinationFolder()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onFileChange(final File file) {
		//		System.out.println(file.getAbsoluteFile() + " was modified.");
		//		System.out.println(Thread.currentThread().getName()+" :: "+">>>------> length: " + file.length());
		//		System.out.println(Thread.currentThread().getName()+" :: "+">>>------> last modified: " + new Date(file.lastModified()));
		//		System.out.println(Thread.currentThread().getName()+" :: "+">>>------> readable: " + file.canRead());
		//		System.out.println(Thread.currentThread().getName()+" :: "+">>>------> writable: " + file.canWrite());
		//		System.out.println(Thread.currentThread().getName()+" :: "+">>>------> executable: " + file.canExecute());
		try {
			String tmpPath = file.getAbsolutePath().replace(backupPreference.getMetadata().getSourceFolder(), "");
			backupPreference.getProvider().copyAFile(file.getAbsolutePath(), backupPreference.getMetadata().getDestinationFolder()+tmpPath);
			//FileUtils.copyFile(new File(file.getAbsolutePath()), new File(backupPreference.getMetadata().getDestinationFolder()+"/"+file.getName()), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onFileDelete(final File file) {
		System.out.println(Thread.currentThread().getName()+" :: "+file.getAbsoluteFile() + " was deleted.");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onStop(final FileAlterationObserver observer) {
		System.out.println(Thread.currentThread().getName()+" :: "+"FileListener has stopped on " + observer.getDirectory().getAbsolutePath());
	}

}
