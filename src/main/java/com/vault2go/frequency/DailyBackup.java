/**
 * 
 */
package com.vault2go.frequency;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import com.vault2go.service.Watcher;

/**
 * @author aswinkumar
 *
 */
public class DailyBackup extends BackupFrequency {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -874314852159864273L;
	
	private int preferredHourInDay = 0;
	
	/**
	 * @throws IOException 
	 * @throws SQLException 
	 * 
	 */
	public DailyBackup() throws IOException, SQLException {
		this.watcherService = new Watcher(getFrequencyInMilliseconds(), TimeUnit.MILLISECONDS);
	}

	@Override
	public String getFrequencyName() {
		return "OnceADayBackup";
	}

	@Override
	public int getFrequencyInMinutes() {
		return 24*60;
	}

	@Override
	public String getUserFriendlyFrequencyName() {
		return "Daily Backup";
	}
	
	public void setPreferredHourInDay(int in) {
		this.preferredHourInDay = in;
	}
	
	public int getPreferredHourInDay() {
		return this.preferredHourInDay;
	}

	@Override
	public int getFrequencyInMilliseconds() {
		return 24*60*60*1000;
	}

}
;