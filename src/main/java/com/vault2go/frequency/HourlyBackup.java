/**
 * 
 */
package com.vault2go.frequency;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import com.vault2go.service.Watcher;

/**
 * @author aswinkumar
 *
 */
public class HourlyBackup extends BackupFrequency {
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 3647911664047794371L;

	public HourlyBackup() throws IOException, SQLException {
		 this.watcherService = new Watcher(getFrequencyInMilliseconds(), TimeUnit.MILLISECONDS);
	}

	@Override
	public String getFrequencyName() {
		return "HourlyBackup";
	}

	@Override
	public int getFrequencyInMinutes() {
		return 60;
	}

	@Override
	public String getUserFriendlyFrequencyName() {
		return "Hourly backup";
	}
	
	@Override
	public int getFrequencyInMilliseconds() {
		return 60*60*1000;
	}

}
