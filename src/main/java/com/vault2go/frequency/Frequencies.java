package com.vault2go.frequency;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Frequencies {
	
	public static List<BackupFrequency> getSupportedFrequencies() throws IOException, SQLException {
		List<BackupFrequency> frequencies = new ArrayList<BackupFrequency>();
		frequencies.add(new DailyBackup());
		frequencies.add(new HourlyBackup());
		frequencies.add(new InstantBackup());
		return frequencies;
	}

	public static BackupFrequency getFrequencyByName(String name) throws IOException, SQLException{
		BackupFrequency out=null;
		for(BackupFrequency frequencyOpt: Frequencies.getSupportedFrequencies()) {
			if(frequencyOpt.getFrequencyName().equalsIgnoreCase(name)) {
				out = frequencyOpt;
				break;
			}
		}
		return out;
	}
}
