/**
 * 
 */
package com.vault2go.frequency;

import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

import com.vault2go.service.Watcher;

/**
 * @author aswinkumar
 *
 */
public class InstantBackup extends BackupFrequency {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3045295806641980921L;

	public InstantBackup() throws IOException, SQLException {
		this.watcherService = new Watcher(getFrequencyInMilliseconds(), TimeUnit.MILLISECONDS);
	}

	@Override
	public String getFrequencyName() {
		return "InstantBackup";
	}

	@Override
	public String getUserFriendlyFrequencyName() {
		return "Instant";
	}

	@Override
	public int getFrequencyInMinutes() {
		return 0;
	}

	@Override
	public int getFrequencyInMilliseconds() {
		return 100;
	}

}
