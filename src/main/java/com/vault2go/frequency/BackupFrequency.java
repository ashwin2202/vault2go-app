package com.vault2go.frequency;

import java.io.Serializable;

import com.vault2go.service.Watcher;

public abstract class BackupFrequency implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected Watcher watcherService;
	
	public abstract String getFrequencyName();
	
	public abstract String getUserFriendlyFrequencyName();
	
	public abstract int getFrequencyInMinutes();
	
	public abstract int getFrequencyInMilliseconds();
	
	public Watcher getWatcherService() {
		return watcherService;
	}

}
