/**
 * 
 */
package com.vault2go;

import com.vault2go.service.Manager;
import com.vault2go.ui.Vault2GoUI;

/**
 * @author aswinkumar
 *
 */
public class Vault2GoStarter {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		Manager manager = Manager.getInstance();
		//manager.startBackupLoop();
		manager.resumeBackup();
		System.out.println("\t\tWelcome to Vault2Go applicaition!!!\n");
		Vault2GoUI.launch(Vault2GoUI.class); // Launch the JavaFX application
	} 

}
