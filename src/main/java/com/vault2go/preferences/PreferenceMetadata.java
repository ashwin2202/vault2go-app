/**
 * 
 */
package com.vault2go.preferences;

import java.io.Serializable;

/**
 * @author aswinkumar
 *
 */
public class PreferenceMetadata implements Serializable{

	/**
	 * @param id
	 * @param sourceFolder
	 * @param frequencyName
	 * @param destinationFolder
	 */
	public PreferenceMetadata(String id, String sourceFolder, String frequencyName, String destinationFolder) {
		super();
		this.id = id;
		this.sourceFolder = sourceFolder;
		this.frequencyName = frequencyName;
		this.destinationFolder = destinationFolder;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -952853535326378081L;

	private String id;

	private String sourceFolder;

	private String frequencyName;

	private String destinationFolder;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the sourceFolder
	 */
	public String getSourceFolder() {
		return sourceFolder;
	}

	/**
	 * @param sourceFolder the sourceFolder to set
	 */
	public void setSourceFolder(String sourceFolder) {
		this.sourceFolder = sourceFolder;
	}

	/**
	 * @return the frequencyName
	 */
	public String getFrequencyName() {
		return frequencyName;
	}

	/**
	 * @param frequencyName the frequencyName to set
	 */
	public void setFrequencyName(String frequencyName) {
		this.frequencyName = frequencyName;
	}

	/**
	 * @return the destinationFolder
	 */
	public String getDestinationFolder() {
		return destinationFolder;
	}

	/**
	 * @param destinationFolder the destinationFolder to set
	 */
	public void setDestinationFolder(String destinationFolder) {
		this.destinationFolder = destinationFolder;
	}

	@Override
	public String toString() {
		return "PreferenceMetadata [" + (id != null ? "id=" + id + ", " : "")
				+ (sourceFolder != null ? "sourceFolder=" + sourceFolder + ", " : "")
				+ (frequencyName != null ? "frequencyName=" + frequencyName + ", " : "")
				+ (destinationFolder != null ? "destinationFolder=" + destinationFolder : "") + "]";
	}
	
}
