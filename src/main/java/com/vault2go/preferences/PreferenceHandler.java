package com.vault2go.preferences;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import com.vault2go.exception.ValidationException;
import com.vault2go.frequency.BackupFrequency;
import com.vault2go.validation.PathValidator;

public class PreferenceHandler {

	private List<BackupFrequency> frequencies;

	private Scanner scanner;

	private PathValidator pathValidator;

	public PreferenceHandler(List<BackupFrequency> frequencies) {
		this.frequencies = frequencies;
		this.scanner = new Scanner(System.in);
		this.pathValidator=new PathValidator();
	}

	public UserBackupPreference createUserPreferences() throws Exception {

		//Source folder
		Path srcFolder = fetchPathFromUser("source");

		//Destination folder
		Path destinationFolder = fetchPathFromUser("destination");

		//Frequency option
		int frequencyIndex = fetchFrequencyIndex();

		UserBackupPreference backupPreference=new UserBackupPreference(srcFolder.toString(), getFrequencies().get(frequencyIndex), destinationFolder.toString());

		getFrequencies().get(frequencyIndex).getWatcherService().addBackupMonitor(backupPreference, false);

		System.out.println(backupPreference);

		return backupPreference;
	}

	/**
	 * @param pathType
	 * @return
	 */
	public Path fetchPathFromUser(String pathType) {
		//Source folder
		Path path = null;
		boolean validPath = false;
		while(!validPath) {
			if("destination".equalsIgnoreCase(pathType)) {
				System.out.println("Please provide the destination directory");
			}
			else {
				System.out.println("Please provide the source directory");
			}
			System.out.println("---------********** HINT **********---------");
			System.out.println("** For Windows machine start with the Drive name. Example: C:/user/folder");
			System.out.println("** For non-Windows machine start with the slash(/). Example: /user/folder");
			System.out.println("---------********** |||| **********---------");
			path = Paths.get(getScanner().next());
			try {
				pathValidator.validatePath(path);
				validPath = true;
			} catch (ValidationException e) {
				System.out.println("\nError Id: "+e.getId()+" | Error Message: "+ e.getMessage()+"\n");
			}
		}	
		return path;
	}

	/**
	 * @return
	 */
	public int fetchFrequencyIndex() {
		//Frequency option
		int frequencyOption = 0;
		boolean frequencySelected = false;
		while(frequencySelected!=true) {
			int count = 0;
			int[] countArr = new int[10];
			for(BackupFrequency frequency: getFrequencies()) {
				countArr[++count] = count;
				System.out.println(count+". "+frequency.getUserFriendlyFrequencyName()+ " runs every "+ frequency.getFrequencyInMinutes()+ " minute/s");
			}

			frequencyOption = getScanner().nextInt();
			if(frequencyOption > 0 && frequencyOption <= getFrequencies().size()) {
				frequencySelected=true;
			}
			else {
				System.out.println("\nInvalid Option Selected. Please try again.\n");
			}
		}
		return frequencyOption-1;
	}
	
	

	/**
	 * @return the frequencies
	 */
	public List<BackupFrequency> getFrequencies() {
		return frequencies;
	}

	/**
	 * @param frequencies the frequencies to set
	 */
	public void setFrequencies(List<BackupFrequency> frequencies) {
		this.frequencies = frequencies;
	}

	/**
	 * @return the scanner
	 */
	public Scanner getScanner() {
		return scanner;
	}

	/**
	 * @param scanner the scanner to set
	 */
	public void setScanner(Scanner scanner) {
		this.scanner = scanner;
	}

	/**
	 * @return the pathValidator
	 */
	public PathValidator getPathValidator() {
		return pathValidator;
	}

	/**
	 * @param pathValidator the pathValidator to set
	 */
	public void setPathValidator(PathValidator pathValidator) {
		this.pathValidator = pathValidator;
	}

}
