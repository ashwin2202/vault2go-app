package com.vault2go.preferences;

import java.io.Serializable;
import java.util.UUID;

import com.vault2go.frequency.BackupFrequency;
import com.vault2go.provider.LocalDeviceBackup;
import com.vault2go.provider.ProviderAware;

public class UserBackupPreference implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4908292025448434909L;

	private ProviderAware provider;
	
	private PreferenceMetadata metadata;
	
	public UserBackupPreference(String sourceFolder, BackupFrequency frequency, String fileExtensions,
			ProviderAware provider, String destinationFolder) {
		super();
		this.provider = provider;
		this.metadata = new PreferenceMetadata(createId(), sourceFolder, frequency.getFrequencyName(), destinationFolder);
	}
	
	//Default Preferences constructor
	public UserBackupPreference(String sourceFolder, BackupFrequency frequency, String destinationFolder) {
		super();
		this.provider = new LocalDeviceBackup();
		this.metadata = new PreferenceMetadata(createId(), sourceFolder, frequency.getFrequencyName(), destinationFolder);
	}
	
	private String createId() {
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        return randomUUIDString;
	}

	public ProviderAware getProvider() {
		return provider;
	}

	public void setProvider(ProviderAware provider) {
		this.provider = provider;
	}

	@Override
	public String toString() {
		return "UserBackupPreference [" + (provider != null ? "provider=" + provider + ", " : "")
				+ (metadata != null ? "metadata=" + metadata : "") + "]";
	}

	/**
	 * @return the metadata
	 */
	public PreferenceMetadata getMetadata() {
		return metadata;
	}

	/**
	 * @param metadata the metadata to set
	 */
	public void setMetadata(PreferenceMetadata metadata) {
		this.metadata = metadata;
	}
	
}
