/**
 * 
 */
package com.vault2go.db.connection;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.ConnectionFactory;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;

/**
 * @author aswinkumar
 *
 */
public class ConnectionManager {

	private static final String CONNECTION_STRING_URI = "jdbc:derby:db/vault2go_db;create=true;user=student622;password=nopasswd";
	
	private static ConnectionManager instance;
	
	private DataSource dataSource;

	private ConnectionManager(){
		System.setProperty("jdbc.drivers", "org.apache.derby.jdbc.EmbeddedDriver");
		ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(CONNECTION_STRING_URI,null);
		PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, null);
		ObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(poolableConnectionFactory);
		poolableConnectionFactory.setPool(connectionPool);
		PoolingDataSource<PoolableConnection> dataSource = new PoolingDataSource<>(connectionPool);
		this.dataSource = dataSource;
	}

	public static synchronized ConnectionManager getInstance(){
		if(instance == null){
			instance = new ConnectionManager();
		}
		return instance;
	}

	/**
	 * @return the dataSource
	 */
	public DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * @param dataSource the dataSource to set
	 */
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
