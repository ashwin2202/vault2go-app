/**
 * 
 */
package com.vault2go.db.helper;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.vault2go.db.connection.ConnectionManager;
import com.vault2go.preferences.PreferenceMetadata;
import com.vault2go.preferences.UserBackupPreference;

/**
 * @author aswinkumar
 *
 */
public class UserPreferenceHelper extends DBHelper{

	private static final String CREATE_TABLE_STMT = 
			"CREATE TABLE vault2go.user_preferences (\n" + 
			"        user_preference_id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1000, INCREMENT BY 1),\n" + 
			"        creation_date DATE NOT NULL,\n" + 
			"        backup_uuid VARCHAR(60) NOT NULL,\n" + 
			"        frequency_name VARCHAR(30) NOT NULL,\n" + 
			"        source_location VARCHAR(255) NOT NULL,\n" + 
			"        destination_location VARCHAR(255) NOT NULL,\n" + 
			"        PRIMARY KEY (user_preference_id)\n" + 
			")";

	private static final String INSERT_PREFERENCE_STMT =
			"INSERT INTO vault2go.user_preferences (backup_uuid, \n" + 
			"                                       creation_date, \n" + 
			"                                       frequency_name, \n" + 
			"                                       source_location, \n" + 
			"                                       destination_location) \n" + 
			"	VALUES (?, ?, ?, ?, ?)";

	private static final String SELECT_PREFERENCE_STMT =
			"SELECT * FROM vault2go.user_preferences \n" + 
			"    WHERE source_location=? \n" + 
			"    AND destination_location=? \n" + 
			"    AND frequency_name=?";
	
	private static final String SELECT_PREFERENCE_STMT_ALL =
			"SELECT * FROM vault2go.user_preferences ORDER BY creation_date";

	public UserPreferenceHelper() throws SQLException {
		super();
		createTableIfMissing(CREATE_TABLE_STMT);
	}

	public boolean saveUserPreference(UserBackupPreference userBackupPreference) throws SQLException {
		try {
			PreparedStatement createPreference = ConnectionManager.getInstance().getDataSource().getConnection().prepareStatement(INSERT_PREFERENCE_STMT);
			createPreference.setString(1, userBackupPreference.getMetadata().getId());
			createPreference.setDate(2, new java.sql.Date(new Date().getTime()));
			createPreference.setString(3, userBackupPreference.getMetadata().getFrequencyName());
			createPreference.setString(4, userBackupPreference.getMetadata().getSourceFolder());
			createPreference.setString(5, userBackupPreference.getMetadata().getDestinationFolder());
			createPreference.execute();
		}
		catch(SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		}
		finally {
			if(conn!=null) {
				conn.close();
			}
		}
		return true;
	}

	public boolean validatePreferenceExists(String frequencyName, String source, String destination) throws SQLException {
		try {
			PreparedStatement selectPreference = ConnectionManager.getInstance().getDataSource().getConnection().prepareStatement(SELECT_PREFERENCE_STMT);
			selectPreference.setString(1, source);
			selectPreference.setString(2, destination);
			selectPreference.setString(3, frequencyName);
			ResultSet rs = selectPreference.executeQuery();
			int counter = 0;
			while(rs.next())
			{
				counter++;
				System.out.println("UUID > "+rs.getString("backup_uuid"));
				System.out.println("Src > "+rs.getString("source_location"));
				System.out.println("Dest > "+rs.getString("destination_location"));
				System.out.println("Frequency name > "+rs.getString("frequency_name"));
			}
			System.out.println("Number of preferences matching with user preference: " + counter);
			if(counter > 0)
				return false;
		}
		catch(SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		}
		finally {
			if(conn!=null) {
				conn.close();
			}
		}
		return true;
	}
	
	public List<PreferenceMetadata> getPreferencesDB() throws SQLException {
		List<PreferenceMetadata> preferencesList = new ArrayList<PreferenceMetadata>();
		try {
			PreparedStatement selectPreference = 
										ConnectionManager
												.getInstance()
												.getDataSource()
												.getConnection()
												.prepareStatement(SELECT_PREFERENCE_STMT_ALL);
			ResultSet rs = selectPreference.executeQuery();
			while(rs.next()){
				preferencesList.add(new PreferenceMetadata(rs.getString("backup_uuid"), 
															rs.getString("source_location"), 
															rs.getString("frequency_name"), 
															rs.getString("destination_location")));
			}
		}
		catch(SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		}
		finally {
			if(conn!=null) {
				conn.close();
			}
		}
		return preferencesList;
	}
	
	public static void main(String[] args) throws SQLException, IOException {
		UserPreferenceHelper helper=new UserPreferenceHelper();
		List<PreferenceMetadata> list = helper.getPreferencesDB();
		for(PreferenceMetadata metadata: list) {
			System.out.println(metadata);
		}
	}
}
