/**
 * 
 */
package com.vault2go.db.helper;

import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.vault2go.db.connection.ConnectionManager;
import com.vault2go.event.Event;
import com.vault2go.event.FileCreationEvent;
import com.vault2go.preferences.PreferenceMetadata;

/**
 * @author aswinkumar
 * 
 * This class is used for managing the activities within activity_history table
 * Operations to create table, save entities and print entries
 *
 */
public class ActivityHelper extends DBHelper{

	public static final String CREATE_TABLE_STMT = 
			"CREATE TABLE vault2go.activity_history (\n" + 
			"    	activity_id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1000, INCREMENT BY 1),\n" + 
			"    	activity_code int NOT NULL,\n" + 
			"    	activity_date date NOT NULL,\n" + 
			"    	description varchar(255) NOT NULL,\n" + 
			"    	primary key (activity_id)\n" +
			")";

	public static final String INSERT_ACTIVITY_STMT =
			"INSERT INTO vault2go.activity_history (\n" + 
			"    	activity_code, \n" + 
			"    	activity_date, \n" + 
			"    	description) \n" + 
			"VALUES \n" + 
			"    (?, ?, ?)";

	public static final String SELECT_ACTIVITY_STMT =
			"SELECT * FROM vault2go.activity_history ORDER BY activity_date";

	public ActivityHelper() throws SQLException {
		super();
		createTableIfMissing(CREATE_TABLE_STMT);
	}

	public boolean saveActivity(Event event) throws SQLException {
		try {
			PreparedStatement createActivity = ConnectionManager
												.getInstance()
												.getDataSource()
												.getConnection()
												.prepareStatement(INSERT_ACTIVITY_STMT);
			createActivity.setInt(1, event.getStatus());
			createActivity.setDate(2, new java.sql.Date(event.getTimestamp().getTime()));
			createActivity.setString(3, event.getDescription());
			createActivity.execute();
		}
		catch(SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		}
		finally {
			if(conn!=null) {
				conn.close();
			}
		}
		return true;
	}
	
	public List<PreferenceMetadata> printActivities() throws SQLException {
		List<PreferenceMetadata> preferencesList = new ArrayList<PreferenceMetadata>();
		try {
			PreparedStatement selectPreference = 
										ConnectionManager
												.getInstance()
												.getDataSource()
												.getConnection()
												.prepareStatement(SELECT_ACTIVITY_STMT);
			ResultSet rs = selectPreference.executeQuery();
			while(rs.next()){
				System.out.println("activityDate: "+rs.getDate("activity_date")
									+"| activityId: "+rs.getString("activity_id")
									+"| Description: "+rs.getString("description"));
			}
		}
		catch(SQLException sqlEx) {
			sqlEx.printStackTrace();
			throw sqlEx;
		}
		finally {
			if(conn!=null) {
				conn.close();
			}
		}
		return preferencesList;
	}

	public static void main(String[] args) throws SQLException {
		ActivityHelper activityHelper=new ActivityHelper();	
		activityHelper.saveActivity(new FileCreationEvent("my_sql.sql", Paths.get("docs").toString()));
		activityHelper.printActivities();
	}
}
