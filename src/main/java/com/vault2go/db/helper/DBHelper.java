package com.vault2go.db.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.vault2go.db.connection.ConnectionManager;

public class DBHelper {
	
	protected Connection conn;
	
	private static final String TABLE_EXISTS_ERR_CODE = "ERROR X0Y32";
	
	public DBHelper() throws SQLException {
		conn = ConnectionManager.getInstance().getDataSource().getConnection();
	}

	public boolean createTableIfMissing(String statement) throws SQLException{
		try {
			Statement createTableStatement = conn.createStatement();
			createTableStatement.execute(statement);
		}
		catch(SQLException sqlEx) {
			if(!sqlEx.getCause().toString().startsWith(TABLE_EXISTS_ERR_CODE)) {
				throw sqlEx;
			}
		}
		finally {
			if(conn!=null) {
				conn.close();
			}
		}
		return true;
	}

	/**
	 * @return the conn
	 */
	public Connection getConn() {
		return conn;
	}

	/**
	 * @param conn the conn to set
	 */
	public void setConn(Connection conn) {
		this.conn = conn;
	}
}
