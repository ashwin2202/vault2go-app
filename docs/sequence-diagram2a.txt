title Vault2Go Background Backup Loop

Vault2GoStarter->+Frequencies: getSupportedFrequencies
loop for each frequency
    Frequencies->+Watcher: createWatcher(polltime)
    Watcher-->-Frequencies: return watcher instance
end
Frequencies-->-Vault2GoStarter: return supportedFrequencies

Vault2GoStarter->+Manager: startBackupLoop
loop for each supported frequence type
    Manager->Worker: createWorker(watcher)
    Manager->Worker: run
    Worker->Watcher: processEvents
    opt if watchevent is received
        Watcher->AcitivtyUpdater: writeActivity
        AcitivtyUpdater->HistoryFile: appendLine
end
